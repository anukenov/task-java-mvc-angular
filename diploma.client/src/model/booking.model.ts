
export class Booking {
  public bookingId:number;
  public numberOfPeople:number|string;
  public recordTime:Date= new Date();
  public recordDateDay:string;
  public recordDateFrom:string;
  public recordDateTo:string;
  public tableType:string;
  public phoneNumber:string;
  public personId:number|string;
}
