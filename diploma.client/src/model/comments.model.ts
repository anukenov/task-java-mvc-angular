
export class Comments {
  public id:number;
  public personId:string;
  public personName:string;
  public itemId:number;
  public itemName:string;
  public messages:string;
  public date:Date;
  public dateFormatter:string;
  public liked:number;
  public disliked:number;
}
