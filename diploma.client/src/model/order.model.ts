export class Order {
  orderId: number;
  orderNo: string;
  personId: string;
  pMethod: string;
  gTotal: number;
  bookingId:number;
  status:number;
}

