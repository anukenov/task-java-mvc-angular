export class UserCans {
  public userCan: string;
  public description: string;
  public actual: number;
}
