export class Item {
  public itemId: number;
  public name: string;
  public price: number;
  public description: string;
  public image: string;
}
