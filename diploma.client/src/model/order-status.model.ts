export class OrderStatus {
  public orderId:number
  public orderItemsId:number;
  public employeeId:number;
  public orderStatus:number;
  public status:number;
  public servedDate:Date;//finish Date when client make bookings
  public updateDate:Date;
}
