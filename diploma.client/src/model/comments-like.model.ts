export class CommentsLike {
  public id:number;
  public personId:string;
  public commentsId:number;
  public liked:number;
  public disliked:number;
}
