export class Languages {
  public RU_LANGUAGE = {
    firstname: 'Назар',
    lastname: 'Абу',
    nameapp: 'E-Order',
    language: 'язык',
    russian: 'Русский',
    english: 'Английский',
    main: 'Главная',
    order: 'Заказ',
    admin: 'Админ',
    registration: 'Регистрация',
    login: 'Войти',
    abouttext: 'Вы можотете тут заказывать еду не вызывая официанта, или заранее забранировать место',
    successadded: 'Успешно добавлено',
    successupdated: 'Успешно добавлено',
    successbooked: 'Успешно забронирован',
    alreadyadded: 'Уже добавлено',
    empty: 'empty',
    full: 'full',
    update: 'Update',
    username: 'Номер пользователя',
    role: "Роль",
    submit: " Отправить",
    fullname: "ФИО",
    productname: "Наименование блюда",
    productprice: "Цена блюда",
    title: "Название",
    category: "Категория",
    date: "Дата",
    food: "Блюдо",
    available: "Доступен",
    notavailable: "Не доступен",
    tablename: "Название столика",
    tableexist: "Существующие столики",
    tablechangename: "Измение название столика",
    tableavailable: "Доступность столика",
    urlfooditem: "food-item",
    urlcustomer: "customer",
    urltable: "table",
    urlmenuday: "menu-day",
    productadd: "Добавить блюдо",
    customeraddrole: "Добавить пользователям роль",
    tableedit: "Изменить столик",
    menudayadd: "Добавить меню-дня",
    responsefromserver: "Ответ от сервера",
    waitresponse: "Ждите ответа",
    placetaken: "Место занято",
    close: "Закрыть",
    exit: "Выйти",
    numberofpeople: "Количество людей",
    reservationday: "День бронирования",
    reservationtimefrom: "Время бронирования с:",
    reservationtimeto: "Время бронирования до:",
    numberoftelephone: "Номер телефона",
    reservationtable: "Бронирования стола:",
    diploma: "diploma",
    loadingwait: "Загрузка, ждите...",
    button: "Войти",
    beforebooking: "Предазаказ",
    orderfooditem: "Заказ блюда",
    booking: "Бронирование",
    canbookingplace: "Вы можете забронировать место или можете еще сделать предзаказ",
    orderno: "Номер заказа",
    customer: "Пользователь",
    pmethod: "Способ оплаты",
    gtotal: "Общий счет",
    createnew: " Создать новый",
    fooditem: "Блюда",
    select: "Выбор",
    price: "Цена",
    quantity: "Количество",
    total: "Всего",
    submitsuccesfully: "Успешно отправлено",
    cash: "Cash",
    card: "Сard",
    granttotal: "Общий счет",
    paymentmethod: "Cпособ оплаты",
    nofooditemselectd: "Для этого заказа не выбран ни один продукт.",
    stillorder: "Eще заказывают:",
    withthisproduct: "C этим продуктом",
    about: "О нас",
    account: 'Аккаунт:',
    status: 'Статус',
    acceptfood: 'Принято',
    telegrambot: 'Телеграм',
    recorddateday: "Забронированный день",
    listorders: "Заказы",
    contactus: "Контакты",
    aboutus: "О нас",
    instruction: "Инструкция",
    choosetable: "Выбрать столик",
    address: "Наурызбай батыра 63, Алматы, КЗ",
    contactustext: "Расскажите нам о своем опыте бронирования! Если у вас есть идеи о том, как мы можем улучшить, мы будем рады услышать от вас. Отправьте нам свой отзыв, заполнив онлайн-запрос внизу страницы, и мы его получим!",
    monday: "Понедельник",
    friday: "Пятница",
    saturday: "Суббота",
    sunday: "Воскресенье",
    goodmorning: "с 9.00 до 24.00",
    launch: "c 9.00 до 22.00",
    evening: "c 10.00 до 24.00",
    openhours: "Время работы",
    officeadresss: "Адрес офиса",
    yourname: 'Ваше имя',
    emailaddress: 'Адресс электронной почты',
    subject: 'Тема',
    typeyourmessage: 'Введите ваше сообщение',
    sendmessage: 'Отправить сообщение',
    message: 'Сообщение',
    email: 'Адрес',
    justname: 'Имя',
    enterin: " Вошли как",
    createdsuccess: "Успешно создан аккаунт! \nТеперь можете войти под своим аккаунтом",
    createdsuccessfail: "Уже зарегестрирова номер. Выберите другой номер.",
    reserved: 'Все права защищены',
    copyright: 'Авторское право 2019',
    anonymous: 'аноним',
    categorytype: 'категории меню',


    abouttext1: 'В Unicorn Pub мы предлагаем отличную качественную еду и приглашаем вас насладиться вкусной кухней.',
    abouttext2: 'Ключ к нашему успеху прост: мы предлагаем высококачественные блюда, которые всегда очень вкусные.\n' +
    '                Мы гордимся тем, что предлагаем нашим клиентам восхитительные и оригинальные блюда, например: Итальянска, Пицца.',
    abouttext3: 'Наслаждайтесь настоящими кулинарными шедеврами. Выберите напиток. Но, прежде всего, расслабьтесь!\n' +
    '                Мы хотели бы поблагодарить Вас за постоянную поддержку.',


    accept: 'Принять',
    acceptend: 'Завершить',
    acceptended: 'Завершено',
    password: 'Пароль пользователя',
    recepientusername: 'Пароль получателя',
    pending: 'В ожидании',
    process: 'В процессе',
    completed: 'Завершено',
    productcategory: "Наименование категории",
    productdescription: "Описание блюда",


    contactmessageemtpty: "Некоторые поля пусты",
    contactmessagefull: "Успешно доставлено, спасибо за ваше внимание",
    bookingInvalid: "Неправильно заполнены поля",

  };
  public EN_LANGUAGE = {
    firstname: 'Anuar',
    lastname: 'Nukenov',
    name: 'Restaurant app.',
    language: 'Language',
    russian: 'Russian',
    english: 'English',
    main: 'Main',
    order: 'Order',
    admin: 'Admin',
    registration: 'Registration',
    login: 'Login',
    productadd: "Add product",
    customeraddrole: "Add customer",
    tableedit: "Edit table",
    menudayadd: "Add menu-day",
    numberofpeople: "Number of people:",
    reservationday: "Reservation day:",
    reservationtimefrom: "Reservation time from:",
    reservationtimeto: "Reservation time to:",
    numberoftelephone: "Number of telephone",
    reservationtable: "Reservation table:",
    diploma: "Diploma",
    orderfooditem: "Order food item",
    orderno: "Order no.",
    customer: "Customer",
    pmethod: "Pay method",
    gtotal: "Total",
    granttotal: "Grand total",
    createnew: "Create new",
    fooditem: "Food item",
    quantity: "Quantity",
    total: "Total",
    submitsuccesfully: "Submitted successfully",
    cash: "Cash",
    card: "Card",
    food: "Food",
    paymentmethod: "Payment method",
    nofooditemselectd: " No food item selected for this order.",

    abouttext: 'You can order food here without calling the waiter, or pre-book a place in advance',
    successadded: 'Successfully added',
    successupdated: 'Successfully updated',
    successbooked: 'Successfully booked',
    alreadyadded: 'Already added',
    empty: 'empty',
    full: 'full',
    update: 'update',
    username: 'Username\'s number',
    role: "Role",
    submit: " Submit",
    fullname: "Full name",
    productname: "Product name",
    productprice: "Product price",
    title: "Name",
    category: "Category",
    date: "Date",
    available: "Available",
    notavailable: "Not available",
    tablename: "Table name",
    tableexist: "Table exist",
    tablechangename: "Add new table ",
    tableavailable: "Change table availabe",
    urlfooditem: "food-item",
    urlcustomer: "customer",
    urltable: "table",
    urlmenuday: "menu-day",
    responsefromserver: "Response from server",
    waitresponse: "Waiting answer",
    placetaken: "Place is taken",
    close: "Close",
    exit: "Exit",
    loadingwait: "Loading, wait...",
    button: "Login",
    beforebooking: "Pre-order",
    booking: "Booking",
    canbookingplace: "You can book a place or pre-order it.",
    select: "Select",
    price: "Price",
    stillorder: "still order:",
    withthisproduct: "With this product",
    about: "About us",
    nameapp: 'E-Order',
    account: 'Account:',
    status: 'Status',
    acceptfood: 'Accept',
    telegrambot: 'Telegram',
    recorddateday: "Booked day",
    listorders: "Orders",
    contactus: "Contact us",
    instruction: "Instruction",
    choosetable: "Choose table",
    contactustext: "Tell us about your booking experience! If you have ideas on how we can improve we'd love to hear from you. Send us your feedback by completing the online enquiry at the bottom of the page and we'll get on it!",
    aboutus: "About us",
    address: "Nauryzbay Batyr 63, Almaty, KZ",
    monday: "Monday",
    friday: "Friday",
    saturday: "Saturday",
    sunday: "Sunday",
    openhours: "Open hours",
    officeadresss: "Office adress",
    yourname: 'Your name',
    emailaddress: 'Email address',
    email: 'Email',
    subject: 'Subject',
    typeyourmessage: 'Type your message',
    message: 'Message',
    sendmessage: 'Send message',
    justname: 'Name',
    enterin: " Logged in as",

    abouttext1: 'At Restaurant Unicorn Pub we offer excellent quality food and invite you to enjoy delicious cuisine.',
    abouttext2: 'The key to our success is simple: we offer high-quality dishes that are always very tasty. We are proud to offer our customers delicious and original dishes, for example: Italian, Pizza.',
    abouttext3: 'Enjoy real culinary masterpieces. Choose a drink. But above all, relax! We would like to thank you for your continued support.',
    goodmorning: "9.00 am to 12 pm",
    launch: "9.00 am to 10 pm",
    evening: "10.00 am to 12 pm",

    password: 'Password',

    accept: 'Accept',
    acceptend: 'Complete',
    acceptended: 'Completed',
    recepientusername: 'Пароль получателя',
    pending: 'Pending',
    process: 'In the process',
    completed: 'Completed',
    reserved: 'All right reserved',
    copyright: 'Copyright 2019 ',
    createdsuccess: "Successfully created an account! \nNow you can log in with your account",
    createdsuccessfail: "Already registered number. Choose another number.",
    anonymous: 'anonymous',
    categorytype: 'menu categories',
    productcategory: "Product category",
    productdescription: "Description of the dish",
    contactmessageemtpty: "Some fields are empty",
    contactmessagefull: "Successfully delivered, thank you for your attention.",
    bookingInvalid: "Incorrectly filled fields",


  }
}
