import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-simple-instruction',
  templateUrl: './simple-instruction.component.html',
  styleUrls: ['./simple-instruction.component.css',
    '../../style/style.css']
})
export class SimpleInstructionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
