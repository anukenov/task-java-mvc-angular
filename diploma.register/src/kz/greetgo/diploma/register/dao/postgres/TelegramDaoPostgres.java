package kz.greetgo.diploma.register.dao.postgres;

import kz.greetgo.depinject.core.Bean;
import kz.greetgo.diploma.register.dao.TelegramDao;

@Bean
public interface TelegramDaoPostgres extends TelegramDao{}
