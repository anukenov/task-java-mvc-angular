package kz.greetgo.diploma.register.dao.postgres;

import kz.greetgo.depinject.core.Bean;
import kz.greetgo.diploma.register.dao.RestaurantOrderDao;

@Bean
public interface RestaurantOrderDaoPostgres extends RestaurantOrderDao {
}
