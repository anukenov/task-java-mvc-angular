package kz.greetgo.diploma.register.dao.postgres;

import kz.greetgo.depinject.core.Bean;
import kz.greetgo.diploma.register.dao.AdminDao;

@Bean
public interface AdminDaoPostgres extends AdminDao {}
