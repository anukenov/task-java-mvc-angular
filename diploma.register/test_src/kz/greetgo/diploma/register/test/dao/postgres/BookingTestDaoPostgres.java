package kz.greetgo.diploma.register.test.dao.postgres;

import kz.greetgo.depinject.core.Bean;
import kz.greetgo.diploma.register.test.dao.BookingTestDao;
import kz.greetgo.diploma.register.test.dao.RestaurantOrderTestDao;

@Bean
public interface BookingTestDaoPostgres extends BookingTestDao {
}
