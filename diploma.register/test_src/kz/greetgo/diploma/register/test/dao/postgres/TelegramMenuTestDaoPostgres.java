package kz.greetgo.diploma.register.test.dao.postgres;

import kz.greetgo.depinject.core.Bean;
import kz.greetgo.diploma.register.test.dao.TelegramMenuTestDao;

@Bean
public interface TelegramMenuTestDaoPostgres extends TelegramMenuTestDao {
}
