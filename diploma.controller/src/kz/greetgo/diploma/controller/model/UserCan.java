package kz.greetgo.diploma.controller.model;



public enum UserCan {
  VIEW_USERS,
  VIEW_ABOUT,
  VIEW_ADMIN,
  VIEW_MANAGER,
  VIEW_STAFF,
  //The following code would be not removed after regenerating
  ///LEAVE_FURTHER
}
