package kz.greetgo.diploma.controller.register.model;

public class ItemCount {
	public Integer itemId;
	public Integer count;

	@Override
	public String toString() {

		return "ItemCount{" +
			"itemId=" + itemId +
			", count=" + count +
			'}';
	}
}
